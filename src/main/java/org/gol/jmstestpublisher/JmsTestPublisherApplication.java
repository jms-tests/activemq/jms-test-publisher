package org.gol.jmstestpublisher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JmsTestPublisherApplication {

    public static void main(String[] args) {
        SpringApplication.run(JmsTestPublisherApplication.class, args);
    }

}
