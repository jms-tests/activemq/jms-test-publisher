package org.gol.jmstestpublisher.controller;

import org.gol.jmstestpublisher.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static java.lang.String.format;
import static java.util.stream.IntStream.range;
import static org.gol.jmstestpublisher.dto.TestMessageDTO.of;

@RestController
@RequestMapping("publish")
public class PublisherController {

    private static final String MESSAGE = "Message no %d";

    @Autowired
    private PublisherService publisherService;

    @GetMapping("queue")
    public HttpEntity<String> sendToQueue(
            @RequestParam(name = "messagesAmount", required = false, defaultValue = "100") Integer messagesAmount
    ) {
        range(1, messagesAmount)
                .forEach(i -> publisherService.sendToQueue(of(i, format(MESSAGE, i))));
        return ResponseEntity.ok("sent");
    }

    @GetMapping("topic")
    public HttpEntity<String> sendToTopic(
            @RequestParam(name = "messagesAmount", required = false, defaultValue = "100") Integer messagesAmount
    ) {
        range(1, messagesAmount)
                .forEach(i -> publisherService.sendToTopic(of(i, format(MESSAGE, i))));
        return ResponseEntity.ok("sent");
    }

    @GetMapping("virtual-topic")
    public HttpEntity<String> sendToVirtualTopic(
            @RequestParam(name = "messagesAmount", required = false, defaultValue = "100") Integer messagesAmount
    ) {
        range(1, messagesAmount)
                .forEach(i -> publisherService.sendToVirtualTopic(of(i, format(MESSAGE, i))));
        return ResponseEntity.ok("sent");
    }
}
